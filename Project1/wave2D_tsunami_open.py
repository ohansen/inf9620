#!/usr/bin/env python
"""
2D wave equation solved by finite differences::

  dt, cpu_time = solver(I, V, f, q, Lx, Ly, Nx, Ny, dt, T, b,
                        user_action=None, version='scalar',
                        dt_safety_factor=1)

Solve the 2D wave equation u_tt = u_xx + u_yy + f(x,t) on (0,L) with
u=0 on the boundary and initial condition du/dt=0.

Nx and Ny are the total number of mesh cells in the x and y
directions. The mesh points are numbered as (0,0), (1,0), (2,0),
..., (Nx,0), (0,1), (1,1), ..., (Nx, Ny).

dt is the time step. If dt<=0, an optimal time step is used.
T is the stop time for the simulation.

I, V, f are functions: I(x,y), V(x,y), f(x,y,t). V and f
can be specified as None or 0, resulting in V=0 and f=0.

user_action: function of (u, x, y, t, n) called at each time
level (x and y are one-dimensional coordinate vectors).
This function allows the calling code to plot the solution,
compute errors, etc.
"""
import time
from scitools.std import *

def solver(I, V, f, c, Lx, Ly, Nx, Ny, dt, T, b,
           user_action=None, version='scalar',
           dt_safety_factor=1):
    if version == 'cython':
        try:
            #import pyximport; pyximport.install()
            import wave2D_u0_loop_cy as compiled_loops
            advance = compiled_loops.advance
        except ImportError, e:
            print 'No module wave2D_u0_loop_cy. Run make_wave2D.sh!'
            print e
            sys.exit(1)
    elif version == 'f77':
        try:
            import wave2D_u0_loop_f77 as compiled_loops
            advance = compiled_loops.advance
        except ImportError:
            print 'No module wave2D_u0_loop_f77. Run make_wave2D.sh!'
            sys.exit(1)
    elif version == 'c_f2py':
        try:
            import wave2D_u0_loop_c_f2py as compiled_loops
            advance = compiled_loops.advance
        except ImportError:
            print 'No module wave2D_u0_loop_c_f2py. Run make_wave2D.sh!'
            sys.exit(1)
    elif version == 'c_cy':
        try:
            import wave2D_u0_loop_c_cy as compiled_loops
            advance = compiled_loops.advance_cwrap
        except ImportError, e:
            print 'No module wave2D_u0_loop_c_cy. Run make_wave2D.sh!'
            print e
            sys.exit(1)
    elif version == 'vectorized':
        advance = advance_vectorized

    x = linspace(0, Lx, Nx+1)  # mesh points in x dir
    y = linspace(0, Ly, Ny+1)  # mesh points in y dir
    dx = x[1] - x[0]
    dy = y[1] - y[0]

    xv = x[:,newaxis]          # for vectorized function evaluations
    yv = y[newaxis,:]

    N = int(round(T/float(dt)))
    t = linspace(0, N*dt, N+1)    # mesh points in time
    Cx2 = (dt/dx)**2;  Cy2 = (dt/dy)**2    # help variables
    #Cx2 = 0;  Cy2 = (dt/dy)**2    # help variables
    dt2 = dt**2

    # Allow f, V to be None or 0
    if f is None or f == 0:
        f = (lambda x, y, t: 0) if version == 'scalar' else \
            lambda x, y, t: zeros((x.shape[0], y.shape[1]))
        # or simpler: x*y*0
    if V is None or V == 0:
        V = (lambda x, y: 0) if version == 'scalar' else \
            lambda x, y: zeros((x.shape[0], y.shape[1]))

#    print 'Cx2*c=', Cx2*c, 'Cy2*c', Cy2*c
    order = 'Fortran' if version == 'f77' else 'C'
    u   = zeros((Nx+1,Ny+1), order=order)   # solution array
    u_1 = zeros((Nx+1,Ny+1), order=order)   # solution at t-dt
    u_2 = zeros((Nx+1,Ny+1), order=order)   # solution at t-2*dt
    f_a = zeros((Nx+1,Ny+1), order=order)   # for compiled loops
    q   = zeros((Nx+1,Ny+1), order=order)   # q=c^2

    import time; t0 = time.clock()          # for measuring CPU time

    # Load initial condition into u_1
    if version == 'scalar':
        for i in range(0, Nx+1):
            for j in range(0, Ny+1):
                u_1[i,j] = I(x[i], y[j])
                q[i,j]   = c[i,j]
    else: # use vectorized version
        u_1[:,:] = I(xv, yv,type='gaussian')
        q[:,:]   = c(xv,yv,type='pulsehill')

    c1=sqrt(max(max(l) for l in q))
    stability_limit = (1/float(c1))*(1/sqrt(1/dx**2 + 1/dy**2))
    if dt <= 0:                # max time step?
        dt = dt_safety_factor*stability_limit
    elif dt > stability_limit:
        print 'error: dt=%g exceeds the stability limit %g' % \
              (dt, stability_limit) 
	
    if user_action is not None:
        user_action(u_1, x, xv, y, yv, t, 0)

    # Special formula for the first time step, with boundary conditions
    n = 0
    if version == 'scalar':
        for i in range(1, Nx):
            for j in range(1, Ny):
                u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
                0.5**2*Cx2*((q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j])) + \
                0.5**2*Cy2*((q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) - (q[i,j]+q[i,j-1])*(u_1[i,j]-u_1[i,j-1])) + \
                0.5*dt2*f(x[i], y[j], t[n])
    #Boundary 
        j = 0
        for i in range(1, Nx):
            u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
                0.5**2*Cx2*((q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j])) + \
                0.5**2*Cy2*(q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j])*2 + \
                0.5*dt2*f(x[i], y[j], t[n])
        j = Ny
        for i in range(1, Nx): 
            u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
                0.5**2*Cx2*((q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j])) + \
                0.5**2*Cy2*(q[i,j]+q[i,j-1])*(u_1[i,j-1]-u_1[i,j])*2 + \
                0.5*dt2*f(x[i], y[j], t[n])
        i = 0
        for j in range(1, Ny):
            u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
                0.5**2*Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j])*2 + \
                0.5**2*Cy2*((q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) - (q[i,j]+q[i,j-1])*(u_1[i,j]-u_1[i,j-1])) + \
                0.5*dt2*f(x[i], y[j], t[n])
        i = Nx
        for j in range(0, Ny+1):
            u[i,j] = 0

        i = 0; j = 0 
        u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
            0.5**2*Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j])*2 + \
            0.5**2*Cy2*(q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j])*2 + \
            0.5*dt2*f(x[i], y[j], t[n])
        i = 0; j = Ny
        u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
            0.5**2*Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j])*2 + \
            0.5**2*Cy2*(q[i,j]+q[i,j-1])*(u_1[i,j-1]-u_1[i,j])*2 + \
            0.5*dt2*f(x[i], y[j], t[n])
    else:  # use vectorized version
        f_a[:,:] = f(xv, yv, t[n])  # precompute, size as u
        V_a = V(xv, yv)
        u[1:-1,1:-1] = u_1[1:-1,1:-1] + dt*V_a[1:-1,1:-1]*(1-b*dt/2) + \
        0.5**2*Cx2*( (q[1:-1,1:-1]+q[2:,1:-1])*(u_1[2:,1:-1]-u_1[1:-1,1:-1]) - (q[1:-1,1:-1]+q[:-2,1:-1])*(u_1[1:-1,1:-1]-u_1[:-2,1:-1]) ) +\
        0.5**2*Cy2*( (q[1:-1,1:-1]+q[1:-1,2:])*(u_1[1:-1,2:]-u_1[1:-1,1:-1]) - (q[1:-1,1:-1]+q[1:-1,:-2])*(u_1[1:-1,1:-1]-u_1[1:-1,:-2])) +\
        0.5*dt2*f_a[1:-1,1:-1]
        #j=0
        u[1:-1 ,0] = u_1[1:-1,0]+dt*V_a[1:-1,0]*(1-b*dt/2) + \
	0.5**2*Cx2*( (q[1:-1,0]+q[2:,0])*(u_1[2:,0]-u_1[1:-1,0]) - (q[1:-1,0]+q[:-2,0])*(u_1[1:-1,0]-u_1[:-2,0]) ) +\
        0.5*Cy2*(q[1:-1,0]+q[1:-1,1])*(u_1[1:-1,1]-u_1[1:-1,0]) +\
        0.5*dt2*f_a[1:-1,0]
        #j=Ny
        u[1:-1,Ny] = u_1[1:-1,Ny] + dt*V_a[1:-1,Ny]*(1-b*dt/2) + \
        0.5**2*Cx2*( (q[1:-1,Ny]+q[2:,Ny])*(u_1[2:,Ny]-u_1[1:-1,Ny]) - (q[1:-1,Ny]+q[:-2,Ny])*(u_1[1:-1,Ny]-u_1[:-2,Ny]) ) +\
        0.5*Cy2*(q[1:-1,Ny]+q[1:-1,Ny-1])*(u_1[1:-1,Ny-1]-u_1[1:-1,Ny]) +\
        0.5*dt2*f_a[1:-1,Ny]
	#i=0
        u[0 ,1:-1] = u_1[0,1:-1] + dt*V_a[0,1:-1]*(1-b*dt/2) + \
        0.5*Cx2*(q[0,1:-1]+q[1,1:-1])*(u_1[1,1:-1]-u_1[0,1:-1]) +\
        0.5**2*Cy2*( (q[0,1:-1]+q[0,2:])*(u_1[0,2:]-u_1[0,1:-1]) - (q[0,1:-1]+q[0,:-2])*(u_1[0,1:-1]-u_1[0,:-2])) +\
        0.5*dt2*f_a[0,1:-1]
	#i=Nx
        u[Nx,:] = 0
        #i = 0; j = Ny
        u[0,Ny] = u_1[0,Ny] + dt*V_a[0,Ny]*(1-b*dt/2) + \
        0.5*Cx2*(q[0,Ny]+q[1,Ny])*(u_1[1,Ny]-u_1[0,Ny]) +\
        0.5*Cy2*(q[0,Ny]+q[0,Ny-1])*(u_1[0,Ny-1]-u_1[0,Ny]) +\
        0.5*dt2*f_a[0,Ny]
        #i = 0; j = 0
        u[0,0] = u_1[0,0] + dt*V_a[0,0]*(1-b*dt/2) + \
        0.5*Cx2*(q[0,0]+q[1,0])*(u_1[1,0]-u_1[0,0]) +\
        0.5*Cy2*(q[0,0]+q[0,1])*(u_1[0,1]-u_1[0,0]) +\
        0.5*dt2*f_a[0,0]

    if user_action is not None:
        user_action(u, x, xv, y, yv, t, 1)

    u_2[:,:] = u_1; u_1[:,:] = u

    for n in range(1, N):
        if version == 'scalar':
            # use f(x,y,t) function
            u = advance_scalar(u, u_1, u_2, f, x, y, t, n,
                               Cx2, Cy2, dt, q, b)
        else:
            f_a[:,:] = f(xv, yv, t[n])  # precompute, size as u
            u = advance(u, u_1, u_2, f_a, Cx2, Cy2, dt, q, b)

        if version == 'f77':
            for a in 'u', 'u_1', 'u_2', 'f_a':
                if not isfortran(eval(a)):
                    print '%s: not Fortran storage!' % a

        if user_action is not None:
            if user_action(u, x, xv, y, yv, t, n+1):
                break

        u_2[:,:], u_1[:,:] = u_1, u

    t1 = time.clock()
    # dt might be computed in this function so return the value
#    return u, dt, t1 - t0
    return u, x, xv, y, yv, t, n, t1 - t0

def advance_scalar(u, u_1, u_2, f, x, y, t, n, Cx2, Cy2, dt, q, b):
    Nx = u.shape[0]-1;  Ny = u.shape[1]-1
    dx=x[1]-x[0]
    for i in range(1, Nx):
        for j in range(1, Ny):
            u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                     0.5*Cx2*( (q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j]) ) + \
                     0.5*Cy2*( (q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) - (q[i,j]+q[i,j-1])*(u_1[i,j]-u_1[i,j-1]) ) + \
                     dt**2*f(x[i], y[j], t[n]) )
    # Boundary conditions
    j = 0
    for i in range(1, Nx): 
        u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                 0.5*Cx2*( (q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j]) ) + \
                 Cy2*(q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) + \
                 dt**2*f(x[i], y[j], t[n]) )
    j = Ny
    for i in range(1, Nx):
            u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                     0.5*Cx2*( (q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j]) ) + \
                     Cy2*(q[i,j]+q[i,j-1])*(u_1[i,j-1]-u_1[i,j]) + \
                     dt**2*f(x[i], y[j], t[n]) )
    i = 0
    for j in range(1, Ny):
            u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                     Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) + \
                     0.5*Cy2*( (q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) - (q[i,j]+q[i,j-1])*(u_1[i,j]-u_1[i,j-1]) ) + \
                     dt**2*f(x[i], y[j], t[n]) )
    i = Nx
    for j in range(0, Ny+1):
            u[i,j] = u_2[i,j]- dt/dx*q[i,j]*(u_1[i,j]-u_1[i-1,j])
    j = 0; i = 0
    u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                 Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) + \
                 Cy2*(q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) + \
                 dt**2*f(x[i], y[j], t[n]) )
    j = Ny; i = 0
    u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                 Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) + \
                 Cy2*(q[i,j]+q[i,j-1])*(u_1[i,j-1]-u_1[i,j]) + \
                 dt**2*f(x[i], y[j], t[n]) )

    return u

def advance_vectorized(u, u_1, u_2, f_a, Cx2, Cy2, dt, q, b):
    Nx = u.shape[0]-1;  Ny = u.shape[1]-1
    dx=dt/sqrt(Cx2)
    u[1:-1,1:-1] = 1/(1+b*dt/2)*( 2*u_1[1:-1,1:-1] - u_2[1:-1,1:-1]*(1-b*dt/2) + \
         0.5*Cx2*( (q[1:-1,1:-1]+q[2:,1:-1])*(u_1[2:,1:-1]-u_1[1:-1,1:-1]) - (q[1:-1,1:-1]+q[:-2,1:-1])*(u_1[1:-1,1:-1]-u_1[:-2,1:-1]) )+ \
         0.5*Cy2*( (q[1:-1,1:-1]+q[1:-1,2:])*(u_1[1:-1,2:]-u_1[1:-1,1:-1]) - (q[1:-1,1:-1]+q[1:-1,:-2])*(u_1[1:-1,1:-1]-u_1[1:-1,:-2]) )+ \
         dt**2*f_a[1:-1,1:-1])
    # j=0
    u[1:-1,0] = 1/(1+b*dt/2)*(2*u_1[1:-1,0] - u_2[1:-1,0]*(1-b*dt/2) + \
         0.5*Cx2*( (q[1:-1,0]+q[2:,0])*(u_1[2:,0]-u_1[1:-1,0]) - (q[1:-1,0]+q[:-2,0])*(u_1[1:-1,0]-u_1[:-2,0])) + \
         Cy2*(q[1:-1,0]+q[1:-1,1])*(u_1[1:-1,1]-u_1[1:-1,0]) + \
         dt**2*f_a[1:-1,0])
    # j=Ny
    u[1:-1,Ny] = 1/(1+b*dt/2)*( 2*u_1[1:-1,Ny] - u_2[1:-1,Ny]*(1-b*dt/2) + \
         0.5*Cx2*( (q[1:-1,Ny]+q[2:,Ny])*(u_1[2:,Ny]-u_1[1:-1,Ny]) - (q[1:-1,Ny]+q[:-2,Ny])*(u_1[1:-1,Ny]-u_1[:-2,Ny]) ) + \
         Cy2*(q[1:-1,Ny]+q[1:-1,Ny-1])*(u_1[1:-1,Ny-1]-u_1[1:-1,Ny]) + \
         dt**2*f_a[1:-1,Ny])
    # i=0
    u[0,1:-1] = 1/(1+b*dt/2)*( 2*u_1[0,1:-1] - u_2[0,1:-1]*(1-b*dt/2) + \
         Cx2*(q[0,1:-1]+q[1,1:-1])*(u_1[1,1:-1]-u_1[0,1:-1]) + \
         0.5*Cy2*((q[0,1:-1]+q[0,2:])*(u_1[0,2:]-u_1[0,1:-1]) - (q[0,1:-1]+q[0,:-2])*(u_1[0,1:-1]-u_1[0,:-2])) + \
         dt**2*f_a[0,1:-1])
    # i=Nx
    u[Nx,:] = u_2[Nx,:] - dt/dx*q[Nx,:]*(u_1[Nx,:]-u_1[Nx-1,:])
    # i=0, j=0
    u[0,0] = 1/(1+b*dt/2)*( 2*u_1[0,0] - u_2[0,0]*(1-b*dt/2) + \
         Cx2*(q[0,0]+q[1,0])*(u_1[1,0]-u_1[0,0]) + \
         Cy2*(q[0,0]+q[0,1])*(u_1[0,1]-u_1[0,0]) + \
         dt**2*f_a[0,0])
    # i=0, j=Ny
    u[0,Ny] = 1/(1+b*dt/2)*( 2*u_1[0,Ny] - u_2[0,Ny]*(1-b*dt/2) + \
         Cx2*(q[0,Ny]+q[1,Ny])*(u_1[1,Ny]-u_1[0,Ny]) + \
         Cy2*(q[0,Ny]+q[0,Ny-1])*(u_1[0,Ny-1]-u_1[0,Ny]) + \
         dt**2*f_a[0,Ny])
    return u

import nose.tools as nt

def test_tsunami(plot_method=2,version='vectorized'):
    # parameters for standwave
    Lx = 10;  Ly = 1
    Nx=50; Ny=1
    T=25
    b=0.0
    dt=0.044

    def I(xv, yv, type=type):
	k=zeros((xv.shape[0],yv.shape[1]))
	if type=='pulse':
            for i in range(0, length(xv)):
	        for j in range(0, length(yv)):
	            if xv[i,0]<1:
		        k[i,j]=1.0
	            else: 
		        k[i,j]=0.0 
	elif type=='gaussian':
	    I0=0
	    Ia=1 # wave height
	    Im=0 # x peak center
	    Is=2 # wave width
	    for i in range(0, length(xv)):
	        for j in range(0, length(yv)):
	            if ((Im-Is)<=xv[i,0]<=(Im+Is)) : 
		        k[i,j]=I0+Ia*exp( -((xv[i,0]-Im)/Is)**2 )
	            else: 
		        k[i,j]=0.0
        return k

    def subsea_hill(xv,yv,type=type):
	b1=1.0 # give gaussian circular contour lines when b=1, else elliptical 
	Bmx=2.0 # peak center
	Bmy=0.5 # peak center
	B0=0.0 
	Bs=1.0 # width
	Ba=0.05 # peak height
	k=zeros((xv.shape[0],yv.shape[1]))
	if type=='gaussian':
            for i in range(0, length(xv)):
	        for j in range(0, length(yv)):
	            if ((Bmx-Bs)<=xv[i,0]<=(Bmx+Bs) and (Bmy-Bs)<=yv[0,j]<=(Bmy+Bs)) : 
		        k[i,j]=B0+Ba*exp( -((xv[i,0]-Bmx)/Bs)**2-((yv[0,j]-Bmy)/(b1*Bs))**2 )
	            else: 
		        k[i,j]=0.0
	elif type=='pulsehill':
	    for i in range(0, length(xv)):
	        for j in range(0, length(yv)):
	            if ((Bmx-Bs)<=xv[i,0]<=(Bmx+Bs) and (Bmy-Bs)<=yv[0,j]<=(Bmy+Bs)) :
		        k[i,j]=Ba
	            else: 
		        k[i,j]=0.0 
	return k

    def q(xv, yv,type=type):
	g=9.81
	H0=0.1
	return g*(H0-subsea_hill(xv,yv,type=type))

    def plot_u(u, x, xv, y, yv, t, n):
        if t[n] == 0:
            time.sleep(2)
        if plot_method == 1:
            plot(xv, u[:,0], title='t=%g' % t[n], ylim=[-.5,1],
                 yaxis=[-.5,1])
        elif plot_method == 2:
            surfc(xv, yv, u, title='t=%g' % t[n], zlim=[-1, 1.8],
                  colorbar=True, colormap=hot(), caxis=[-1,1.8],
                  shading='flat')
        if plot_method > 0:
            time.sleep(0) # pause between frames
            filename = 'tmp_%04d.png' % n
            #savefig(filename)  # time consuming - dropped


    # Note: problem with nosetests - some module import problem
    def assert_no_error(u, x, xv, y, yv, t, n):
	plot_u(u, x, xv, y, yv, t, n)
    
    u, x, xv, y, yv, t, n, cpu = solver(I, None, None, q, Lx, Ly, Nx, Ny, dt, T, b,
                         user_action=None, #assert_no_error,
                         version=version)

    plot_u(u, x, xv, y, yv, t, n)

if __name__ == '__main__':
    import sys
    from scitools.misc import function_UI
    cmd = function_UI([test_quadratic, run_efficiency_tests,
                       run_Gaussian, run_Test], sys.argv)
    eval(cmd)
