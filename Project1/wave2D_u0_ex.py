#!/usr/bin/env python
"""
2D wave equation solved by finite differences::

  dt, cpu_time = solver(I, V, f, q, Lx, Ly, Nx, Ny, dt, T, b,
                        user_action=None, version='scalar',
                        dt_safety_factor=1)

Solve the 2D wave equation u_tt = u_xx + u_yy + f(x,t) on (0,L) with
u=0 on the boundary and initial condition du/dt=0.

Nx and Ny are the total number of mesh cells in the x and y
directions. The mesh points are numbered as (0,0), (1,0), (2,0),
..., (Nx,0), (0,1), (1,1), ..., (Nx, Ny).

dt is the time step. If dt<=0, an optimal time step is used.
T is the stop time for the simulation.

I, V, f are functions: I(x,y), V(x,y), f(x,y,t). V and f
can be specified as None or 0, resulting in V=0 and f=0.

user_action: function of (u, x, y, t, n) called at each time
level (x and y are one-dimensional coordinate vectors).
This function allows the calling code to plot the solution,
compute errors, etc.
"""
import time
from scitools.std import *

def solver(I, V, f, c, Lx, Ly, Nx, Ny, dt, T, b,
           user_action=None, version='scalar',
           dt_safety_factor=1):
    if version == 'cython':
        try:
            #import pyximport; pyximport.install()
            import wave2D_u0_loop_cy as compiled_loops
            advance = compiled_loops.advance
        except ImportError, e:
            print 'No module wave2D_u0_loop_cy. Run make_wave2D.sh!'
            print e
            sys.exit(1)
    elif version == 'f77':
        try:
            import wave2D_u0_loop_f77 as compiled_loops
            advance = compiled_loops.advance
        except ImportError:
            print 'No module wave2D_u0_loop_f77. Run make_wave2D.sh!'
            sys.exit(1)
    elif version == 'c_f2py':
        try:
            import wave2D_u0_loop_c_f2py as compiled_loops
            advance = compiled_loops.advance
        except ImportError:
            print 'No module wave2D_u0_loop_c_f2py. Run make_wave2D.sh!'
            sys.exit(1)
    elif version == 'c_cy':
        try:
            import wave2D_u0_loop_c_cy as compiled_loops
            advance = compiled_loops.advance_cwrap
        except ImportError, e:
            print 'No module wave2D_u0_loop_c_cy. Run make_wave2D.sh!'
            print e
            sys.exit(1)
    elif version == 'vectorized':
        advance = advance_vectorized

    x = linspace(0, Lx, Nx+1)  # mesh points in x dir
    y = linspace(0, Ly, Ny+1)  # mesh points in y dir
    dx = x[1] - x[0]
    dy = y[1] - y[0]

    xv = x[:,newaxis]          # for vectorized function evaluations
    yv = y[newaxis,:]

    N = int(round(T/float(dt)))
    t = linspace(0, N*dt, N+1)    # mesh points in time
    Cx2 = (dt/dx)**2;  Cy2 = (dt/dy)**2    # help variables
    #Cx2 = 0;  Cy2 = (dt/dy)**2    # help variables
    dt2 = dt**2

    # Allow f, V to be None or 0
    if f is None or f == 0:
        f = (lambda x, y, t: 0) if version == 'scalar' else \
            lambda x, y, t: zeros((x.shape[0], y.shape[1]))
        # or simpler: x*y*0
    if V is None or V == 0:
        V = (lambda x, y: 0) if version == 'scalar' else \
            lambda x, y: zeros((x.shape[0], y.shape[1]))


#    c=sqrt(max(q_1))
#    stability_limit = (1/float(c))*(1/sqrt(1/dx**2 + 1/dy**2))
#    if dt <= 0:                # max time step?
#        dt = dt_safety_factor*stability_limit
#    elif dt > stability_limit:
#        print 'error: dt=%g exceeds the stability limit %g' % \
#              (dt, stability_limit) 
#

#    print 'Cx2*c=', Cx2*c, 'Cy2*c', Cy2*c
    order = 'Fortran' if version == 'f77' else 'C'
    u   = zeros((Nx+1,Ny+1), order=order)   # solution array
    u_1 = zeros((Nx+1,Ny+1), order=order)   # solution at t-dt
    u_2 = zeros((Nx+1,Ny+1), order=order)   # solution at t-2*dt
    f_a = zeros((Nx+1,Ny+1), order=order)   # for compiled loops
    q   = zeros((Nx+1,Ny+1), order=order)   # q=c^2

    import time; t0 = time.clock()          # for measuring CPU time

    # Load initial condition into u_1
    if version == 'scalar':
        for i in range(0, Nx+1):
            for j in range(0, Ny+1):
                u_1[i,j] = I(x[i], y[j])
                q[i,j]   = c[i,j]
    else: # use vectorized version
        u_1[:,:] = I(xv, yv)
        q[:,:]   = c(xv,yv)
	
    print q,u_1
    if user_action is not None:
        user_action(u_1, x, xv, y, yv, t, 0)

    # Special formula for the first time step, with boundary conditions
    n = 0
    if version == 'scalar':
        for i in range(1, Nx):
            for j in range(1, Ny):
                u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
                0.5**2*Cx2*((q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j])) + \
                0.5**2*Cy2*((q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) - (q[i,j]+q[i,j-1])*(u_1[i,j]-u_1[i,j-1])) + \
                0.5*dt2*f(x[i], y[j], t[n])
    #Boundary 
        j = 0
        for i in range(1, Nx):
            u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
                0.5**2*Cx2*((q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j])) + \
                0.5**2*Cy2*(q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j])*2 + \
                0.5*dt2*f(x[i], y[j], t[n])
        j = Ny
        for i in range(1, Nx): 
            u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
                0.5**2*Cx2*((q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j])) + \
                0.5**2*Cy2*(q[i,j]+q[i,j-1])*(u_1[i,j-1]-u_1[i,j])*2 + \
                0.5*dt2*f(x[i], y[j], t[n])
        i = 0
        for j in range(1, Ny):
            u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
                0.5**2*Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j])*2 + \
                0.5**2*Cy2*((q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) - (q[i,j]+q[i,j-1])*(u_1[i,j]-u_1[i,j-1])) + \
                0.5*dt2*f(x[i], y[j], t[n])
        i = Nx
        for j in range(1, Ny):
            u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
                0.5**2*Cx2*(q[i,j]+q[i-1,j])*(u_1[i-1,j]-u_1[i,j])*2 + \
                0.5**2*Cy2*((q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) - (q[i,j]+q[i,j-1])*(u_1[i,j]-u_1[i,j-1])) + \
                0.5*dt2*f(x[i], y[j], t[n])
        i = Nx; j = 0 
        u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
            0.5**2*Cx2*(q[i,j]+q[i-1,j])*(u_1[i-1,j]-u_1[i,j])*2 + \
            0.5**2*Cy2*(q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j])*2 + \
            0.5*dt2*f(x[i], y[j], t[n])
        i = 0; j = 0 
        u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
            0.5**2*Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j])*2 + \
            0.5**2*Cy2*(q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j])*2 + \
            0.5*dt2*f(x[i], y[j], t[n])
        i = 0; j = Ny
        u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
            0.5**2*Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j])*2 + \
            0.5**2*Cy2*(q[i,j]+q[i,j-1])*(u_1[i,j-1]-u_1[i,j])*2 + \
            0.5*dt2*f(x[i], y[j], t[n])
        i = Nx; j = Ny
        u[i,j] = u_1[i,j] + dt*V(x[i], y[j])*(1-b*dt/2) + \
            0.5**2*Cx2*(q[i,j]+q[i-1,j])*(u_1[i-1,j]-u_1[i,j])*2 + \
            0.5**2*Cy2*(q[i,j]+q[i,j-1])*(u_1[i,j-1]-u_1[i,j])*2 + \
            0.5*dt2*f(x[i], y[j], t[n])
    else:  # use vectorized version
        f_a[:,:] = f(xv, yv, t[n])  # precompute, size as u
        V_a = V(xv, yv)
        u[1:-1,1:-1] = u_1[1:-1,1:-1] + dt*V_a[1:-1,1:-1]*(1-b*dt/2) + \
        0.5**2*Cx2*( (q[1:-1,1:-1]+q[2:,1:-1])*(u_1[2:,1:-1]-u_1[1:-1,1:-1]) - (q[1:-1,1:-1]+q[:-2,1:-1])*(u_1[1:-1,1:-1]-u_1[:-2,1:-1]) ) +\
        0.5**2*Cy2*( (q[1:-1,1:-1]+q[1:-1,2:])*(u_1[1:-1,2:]-u_1[1:-1,1:-1]) - (q[1:-1,1:-1]+q[1:-1,:-2])*(u_1[1:-1,1:-1]-u_1[1:-1,:-2])) +\
        0.5*dt2*f_a[1:-1,1:-1]
        #j=0
        u[1:-1 ,0] = u_1[1:-1,0]+dt*V_a[1:-1,0]*(1-b*dt/2) + \
	0.5**2*Cx2*( (q[1:-1,0]+q[2:,0])*(u_1[2:,0]-u_1[1:-1,0]) - (q[1:-1,0]+q[:-2,0])*(u_1[1:-1,0]-u_1[:-2,0]) ) +\
        0.5*Cy2*(q[1:-1,0]+q[1:-1,1])*(u_1[1:-1,1]-u_1[1:-1,0]) +\
        0.5*dt2*f_a[1:-1,0]
        #j=Ny
        u[1:-1,Ny] = u_1[1:-1,Ny] + dt*V_a[1:-1,Ny]*(1-b*dt/2) + \
        0.5**2*Cx2*( (q[1:-1,Ny]+q[2:,Ny])*(u_1[2:,Ny]-u_1[1:-1,Ny]) - (q[1:-1,Ny]+q[:-2,Ny])*(u_1[1:-1,Ny]-u_1[:-2,Ny]) ) +\
        0.5*Cy2*(q[1:-1,Ny]+q[1:-1,Ny-1])*(u_1[1:-1,Ny-1]-u_1[1:-1,Ny]) +\
        0.5*dt2*f_a[1:-1,Ny]
	#i=0
        u[0 ,1:-1] = u_1[0,1:-1] + dt*V_a[0,1:-1]*(1-b*dt/2) + \
        0.5*Cx2*(q[0,1:-1]+q[1,1:-1])*(u_1[1,1:-1]-u_1[0,1:-1]) +\
        0.5**2*Cy2*( (q[0,1:-1]+q[0,2:])*(u_1[0,2:]-u_1[0,1:-1]) - (q[0,1:-1]+q[0,:-2])*(u_1[0,1:-1]-u_1[0,:-2])) +\
        0.5*dt2*f_a[0,1:-1]
	#i=Nx
        u[Nx,1:-1] = u_1[Nx,1:-1] + dt*V_a[Nx,1:-1]*(1-b*dt/2) + \
        0.5*Cx2*(q[Nx,1:-1]+q[Nx-1,1:-1])*(u_1[Nx-1,1:-1]-u_1[Nx,1:-1]) +\
        0.5**2*Cy2*( (q[Nx,1:-1]+q[Nx,2:])*(u_1[Nx,2:]-u_1[Nx,1:-1]) - (q[Nx,1:-1]+q[Nx,:-2])*(u_1[Nx,1:-1]-u_1[Nx,:-2])) +\
        0.5*dt2*f_a[Nx,1:-1]
        #i = Nx; j = 0
        u[Nx,0] = u_1[Nx,0] + dt*V_a[Nx,0]*(1-b*dt/2) + \
        0.5*Cx2*(q[Nx,0]+q[Nx-1,0])*(u_1[Nx-1,0]-u_1[Nx,0]) +\
        0.5*Cy2*(q[Nx,0]+q[Nx,1])*(u_1[Nx,1]-u_1[Nx,0]) +\
        0.5*dt2*f_a[Nx,0]
        #i = 0; j = Ny
        u[0,Ny] = u_1[0,Ny] + dt*V_a[0,Ny]*(1-b*dt/2) + \
        0.5*Cx2*(q[0,Ny]+q[1,Ny])*(u_1[1,Ny]-u_1[0,Ny]) +\
        0.5*Cy2*(q[0,Ny]+q[0,Ny-1])*(u_1[0,Ny-1]-u_1[0,Ny]) +\
        0.5*dt2*f_a[0,Ny]
        #i = 0; j = 0
        u[0,0] = u_1[0,0] + dt*V_a[0,0]*(1-b*dt/2) + \
        0.5*Cx2*(q[0,0]+q[1,0])*(u_1[1,0]-u_1[0,0]) +\
        0.5*Cy2*(q[0,0]+q[0,1])*(u_1[0,1]-u_1[0,0]) +\
        0.5*dt2*f_a[0,0]
        #i = Nx; j = Ny
        u[Nx,Ny] = u_1[Nx,Ny] + dt*V_a[Nx,Ny]*(1-b*dt/2) + \
        0.5*Cx2*(q[Nx,Ny]+q[Nx-1,Ny])*(u_1[Nx-1,Ny]-u_1[Nx,Ny]) +\
        0.5*Cy2*(q[Nx,Ny]+q[Nx,Ny-1])*(u_1[Nx,Ny-1]-u_1[Nx,Ny]) +\
        0.5*dt2*f_a[Nx,Ny]

    if user_action is not None:
        user_action(u, x, xv, y, yv, t, 1)

    u_2[:,:] = u_1; u_1[:,:] = u

    for n in range(1, N):
        if version == 'scalar':
            # use f(x,y,t) function
            u = advance_scalar(u, u_1, u_2, f, x, y, t, n,
                               Cx2, Cy2, dt, q, b)
        else:
            f_a[:,:] = f(xv, yv, t[n])  # precompute, size as u
            u = advance(u, u_1, u_2, f_a, Cx2, Cy2, dt, q, b)

        if version == 'f77':
            for a in 'u', 'u_1', 'u_2', 'f_a':
                if not isfortran(eval(a)):
                    print '%s: not Fortran storage!' % a

        if user_action is not None:
            if user_action(u, x, xv, y, yv, t, n+1):
                break

        u_2[:,:], u_1[:,:] = u_1, u

    t1 = time.clock()
    # dt might be computed in this function so return the value
#    return u, dt, t1 - t0
    return u, xv, yv, t, t1 - t0

def advance_scalar(u, u_1, u_2, f, x, y, t, n, Cx2, Cy2, dt, q, b):
    Nx = u.shape[0]-1;  Ny = u.shape[1]-1
    for i in range(1, Nx):
        for j in range(1, Ny):
            u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                     0.5*Cx2*( (q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j]) ) + \
                     0.5*Cy2*( (q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) - (q[i,j]+q[i,j-1])*(u_1[i,j]-u_1[i,j-1]) ) + \
                     dt**2*f(x[i], y[j], t[n]) )
    # Boundary conditions
    j = 0
    for i in range(1, Nx): 
        u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                 0.5*Cx2*( (q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j]) ) + \
                 Cy2*(q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) + \
                 dt**2*f(x[i], y[j], t[n]) )
    j = Ny
    for i in range(1, Nx):
            u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                     0.5*Cx2*( (q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) - (q[i,j]+q[i-1,j])*(u_1[i,j]-u_1[i-1,j]) ) + \
                     Cy2*(q[i,j]+q[i,j-1])*(u_1[i,j-1]-u_1[i,j]) + \
                     dt**2*f(x[i], y[j], t[n]) )
    i = 0
    for j in range(1, Ny):
            u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                     Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) + \
                     0.5*Cy2*( (q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) - (q[i,j]+q[i,j-1])*(u_1[i,j]-u_1[i,j-1]) ) + \
                     dt**2*f(x[i], y[j], t[n]) )
    i = Nx
    for j in range(1, Ny):
            u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                     Cx2*(q[i,j]+q[i-1,j])*(u_1[i-1,j]-u_1[i,j]) + \
                     0.5*Cy2*( (q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) - (q[i,j]+q[i,j-1])*(u_1[i,j]-u_1[i,j-1]) ) + \
                     dt**2*f(x[i], y[j], t[n]) )
    j = 0; i = 0
    u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                 Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) + \
                 Cy2*(q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) + \
                 dt**2*f(x[i], y[j], t[n]) )
    j = 0; i = Nx
    u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                 Cx2*(q[i,j]+q[i-1,j])*(u_1[i-1,j]-u_1[i,j]) + \
                 Cy2*(q[i,j]+q[i,j+1])*(u_1[i,j+1]-u_1[i,j]) + \
                 dt**2*f(x[i], y[j], t[n]) )
    j = Ny; i = 0
    u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                 Cx2*(q[i,j]+q[i+1,j])*(u_1[i+1,j]-u_1[i,j]) + \
                 Cy2*(q[i,j]+q[i,j-1])*(u_1[i,j-1]-u_1[i,j]) + \
                 dt**2*f(x[i], y[j], t[n]) )
    j = Ny; i = Nx
    u[i,j] = 1/(1+b*dt/2)*( 2*u_1[i,j] - u_2[i,j]*(1-b*dt/2) + \
                 Cx2*(q[i,j]+q[i-1,j])*(u_1[i-1,j]-u_1[i,j]) + \
                 Cy2*(q[i,j]+q[i,j-1])*(u_1[i,j-1]-u_1[i,j]) + \
                 dt**2*f(x[i], y[j], t[n]) )
    return u

def advance_vectorized(u, u_1, u_2, f_a, Cx2, Cy2, dt, q, b):
    Nx = u.shape[0]-1;  Ny = u.shape[1]-1
    u[1:-1,1:-1] = 1/(1+b*dt/2)*( 2*u_1[1:-1,1:-1] - u_2[1:-1,1:-1]*(1-b*dt/2) + \
         0.5*Cx2*( (q[1:-1,1:-1]+q[2:,1:-1])*(u_1[2:,1:-1]-u_1[1:-1,1:-1]) - (q[1:-1,1:-1]+q[:-2,1:-1])*(u_1[1:-1,1:-1]-u_1[:-2,1:-1]) )+ \
         0.5*Cy2*( (q[1:-1,1:-1]+q[1:-1,2:])*(u_1[1:-1,2:]-u_1[1:-1,1:-1]) - (q[1:-1,1:-1]+q[1:-1,:-2])*(u_1[1:-1,1:-1]-u_1[1:-1,:-2]) )+ \
         dt**2*f_a[1:-1,1:-1])
    # j=0
    u[1:-1,0] = 1/(1+b*dt/2)*(2*u_1[1:-1,0] - u_2[1:-1,0]*(1-b*dt/2) + \
         0.5*Cx2*( (q[1:-1,0]+q[2:,0])*(u_1[2:,0]-u_1[1:-1,0]) - (q[1:-1,0]+q[:-2,0])*(u_1[1:-1,0]-u_1[:-2,0])) + \
         Cy2*(q[1:-1,0]+q[1:-1,1])*(u_1[1:-1,1]-u_1[1:-1,0]) + \
         dt**2*f_a[1:-1,0])
    # j=Ny
    u[1:-1,Ny] = 1/(1+b*dt/2)*( 2*u_1[1:-1,Ny] - u_2[1:-1,Ny]*(1-b*dt/2) + \
         0.5*Cx2*( (q[1:-1,Ny]+q[2:,Ny])*(u_1[2:,Ny]-u_1[1:-1,Ny]) - (q[1:-1,Ny]+q[:-2,Ny])*(u_1[1:-1,Ny]-u_1[:-2,Ny]) ) + \
         Cy2*(q[1:-1,Ny]+q[1:-1,Ny-1])*(u_1[1:-1,Ny-1]-u_1[1:-1,Ny]) + \
         dt**2*f_a[1:-1,Ny])
    # i=0
    u[0,1:-1] = 1/(1+b*dt/2)*( 2*u_1[0,1:-1] - u_2[0,1:-1]*(1-b*dt/2) + \
         Cx2*(q[0,1:-1]+q[1,1:-1])*(u_1[1,1:-1]-u_1[0,1:-1]) + \
         0.5*Cy2*((q[0,1:-1]+q[0,2:])*(u_1[0,2:]-u_1[0,1:-1]) - (q[0,1:-1]+q[0,:-2])*(u_1[0,1:-1]-u_1[0,:-2])) + \
         dt**2*f_a[0,1:-1])
    u[Nx,1:-1] = 1/(1+b*dt/2)*( 2*u_1[Nx,1:-1] - u_2[Nx,1:-1]*(1-b*dt/2) + \
         Cx2*(q[Nx,1:-1]+q[Nx-1,1:-1])*(u_1[Nx-1,1:-1]-u_1[Nx,1:-1]) + \
         0.5*Cy2*((q[Nx,1:-1]+q[Nx,2:])*(u_1[Nx,2:]-u_1[Nx,1:-1]) - (q[Nx,1:-1]+q[Nx,:-2])*(u_1[Nx,1:-1]-u_1[Nx,:-2])) + \
         dt**2*f_a[Nx,1:-1])
    # i=0, j=0
    u[0,0] = 1/(1+b*dt/2)*( 2*u_1[0,0] - u_2[0,0]*(1-b*dt/2) + \
         Cx2*(q[0,0]+q[1,0])*(u_1[1,0]-u_1[0,0]) + \
         Cy2*(q[0,0]+q[0,1])*(u_1[0,1]-u_1[0,0]) + \
         dt**2*f_a[0,0])
    # i=Nx, j=0
    u[Nx,0] = 1/(1+b*dt/2)*( 2*u_1[Nx,0] - u_2[Nx,0]*(1-b*dt/2) + \
         Cx2*(q[Nx,0]+q[Nx-1,0])*(u_1[Nx-1,0]-u_1[Nx,0]) + \
         Cy2*(q[Nx,0]+q[Nx,1])*(u_1[Nx,1]-u_1[Nx,0]) + \
         dt**2*f_a[Nx,0])
    # i=0, j=Ny
    u[0,Ny] = 1/(1+b*dt/2)*( 2*u_1[0,Ny] - u_2[0,Ny]*(1-b*dt/2) + \
         Cx2*(q[0,Ny]+q[1,Ny])*(u_1[1,Ny]-u_1[0,Ny]) + \
         Cy2*(q[0,Ny]+q[0,Ny-1])*(u_1[0,Ny-1]-u_1[0,Ny]) + \
         dt**2*f_a[0,Ny])
    # i=Nx, j=Ny
    u[Nx,Ny] = 1/(1+b*dt/2)*( 2*u_1[Nx,Ny] - u_2[Nx,Ny]*(1-b*dt/2) + \
         Cx2*(q[Nx,Ny]+q[Nx-1,Ny])*(u_1[Nx-1,Ny]-u_1[Nx,Ny]) + \
         Cy2*(q[Nx,Ny]+q[Nx,Ny-1])*(u_1[Nx,Ny-1]-u_1[Nx,Ny]) + \
         dt**2*f_a[Nx,Ny])
    return u

import nose.tools as nt

def test_standwave(plot_method=2,version='scalar'):
    # parameters for standwave
    Lx = 10;  Ly = 10
    Nx=150; Ny=150
    mx=1; my=1
    freq = 1
    #error on stand wave
    h=float(Lx)/Nx
    Ft=0.01
    Fx=1
    Fy=1
    dt=float(Ft)*h
    Dx=float(Fx)*h
    Dy=float(Fy)*h
    
    lambda_x=2*Lx/mx
    lambda_y=lambda_x
    w=2*pi*freq
#    T = 2*pi/lambda_x
    T=0.1
    c=freq*lambda_x
    b=0.01
    c_x=int(mx)*pi/Lx
    c_y=int(my)*pi/Ly
    u_e = zeros((Nx+1,Ny+1))

    def solution(x, y, t):
        return exp(-b*t)*cos(c_x*x)*cos(c_y*y)*cos(w*t)

    def I(x, y):
        return solution(x, y, 0)

    def V(x, y):
        return -b*solution(x, y, 0)

    def f(x, y, t):
        #return 0
        return b*w*exp(-b*t)*cos(c_x*x)*cos(c_y*y)*sin(w*t) + solution(x, y, t)*(q_1*(c_x**2+c_y**2)-w**2)

    q_1=c**2

    def plot_u(u, x, xv, y, yv, t, n):
        if t[n] == 0:
            time.sleep(2)
        if plot_method == 1:
            mesh(x, y, u, title='t=%g' % t[n], zlim=[-1,1],
                 caxis=[-1,1])
        elif plot_method == 2:
            surfc(xv, yv, u, title='t=%g' % t[n],
                  colorbar=True, colormap=hot(),
                  shading='flat')
        if plot_method > 0:
            time.sleep(0) # pause between frames
            filename = 'tmp_%04d.png' % n
            #savefig(filename)  # time consuming - dropped

    # Note: problem with nosetests - some module import problem
    def assert_no_error(u, x, xv, y, yv, t, n):
	for i in range(0, Nx+1):
            for j in range(0, Ny+1):
		for k in range(0, n+1):
    	            u_e[i,j] = solution(x[i], y[j], t[k])
#        diff = abs(u - u_e).max()
        #print n, version, diff
#        nt.assert_almost_equal(diff, 0, places=12)
#	plot_u(u, x, xv, y, yv, t, n)
	figure()
        plot(x,u[:,25],'r--o')
        hold(True)
        plot(x,u_e[:,25],'b-')
        legend(['num', 'exact'])
        xlabel('x')
        ylabel('u')

    u, x, y, t, cpu = solver(I, V, f, q_1, Lx, Ly, Nx, Ny, dt, T, b,
                         user_action=None,
                         version=version)

    for i in range(0, Nx+1):
        for j in range(0, Ny+1):
    	    u_e[i,j] = solution(x[i], y[j], t[-1])
    figure()
    plot(x,u[:,2],'r--o')
    hold(True)
    plot(x,u_e[:,2],'b-')
    legend(['num', 'exact'])
    xlabel('x')
    ylabel('u')
    e=u_e-u
    E=sqrt(dt*sum(e**2))
    
    

#    plot_u(u_e, x, xv, y, yv, t, n)
	    
def test_standwaveVec(plot_method=2,version='vectorized'):
    # parameters for standwave
    Lx = 10;  Ly = 10
    mx=1; my=1
    freq = 1
    lambda_x=2*Lx/mx
    lambda_y=lambda_x
    w=2*pi*freq
    T=0.01
    c=freq*lambda_x
    b=0.01
    c_x=int(mx)*pi/Lx
    c_y=int(my)*pi/Ly
#    u_e = zeros((Nx+1,Ny+1))

    def solution(xv, yv, t):
        return exp(-b*t)*cos(c_x*xv)*cos(c_y*yv)*cos(w*t)

    def I(xv, yv):
        return solution(xv, yv, 0)

    def V(xv, yv):
        return -b*solution(xv, yv, 0)

    def f(xv, yv, t):
        return b*w*exp(-b*t)*cos(c_x*xv)*cos(c_y*yv)*sin(w*t) + solution(xv, yv, t)*(q_1*(c_x**2+c_y**2)-w**2)

    q_1=c**2

    def plot_u(u, x, xv, y, yv, t, n):
        if t[n] == 0:
            time.sleep(2)
        if plot_method == 1:
            mesh(x, y, u, title='t=%g' % t[n], zlim=[-1,1],
                 caxis=[-1,1])
        elif plot_method == 2:
            surfc(x, y, u, title='t=%g' % t[n], zlim=[-1, 1],
                  colorbar=True, colormap=hot(), caxis=[-1,1],
                  shading='flat')
        if plot_method > 0:
            time.sleep(0) # pause between frames
            filename = 'tmp_%04d.png' % n
            #savefig(filename)  # time consuming - dropped

    # Note: problem with nosetests - some module import problem
    def assert_no_error(u, x, xv, y, yv, t, n):
	plot_u(u, x, xv, y, yv, t, n)


#    dt=array([0.0001,0.0005, 0.005, 0.001, 0.05, 0.01, 0.05, 0.1])
    dt=0.01
    Ft=0.01
#    h=zeros(len(dt))
    h=dt/Ft
    Dx=h
    Dy=h
    Fx=Dx/h
    Fy=Dy/h
    Nx=Lx/Dx
    Ny=Ly/Dy
    E=[]
    u, xv, yv, t, cpu = solver(I, V, f, q_1, Lx, Ly, Nx, Ny, dt, T, b,
                         user_action=assert_no_error,
                         version=version)
    def test_conv(dt):
        for d in range(0,len(dt)):
	    u_e = zeros((Nx[d]+1,Ny[d]+1))
	    u, xv, yv, t, cpu = solver(I, V, f, q_1, Lx, Ly, Nx[d], Ny[d], dt[d], T, b,
                         user_action=None,
                         version=version)
            u_e[:,:] = solution(xv, yv, t[-1])
	    diff=sqrt(sum((u_e-u)**2))
	    E.append(diff)
        m=len(dt)
        dim=[log(E[p-1]/E[p])/log(dt[p-1]/dt[p]) for p in range(1, m ,1)]
        print dim

def test_tsunami(plot_method=2,version='vectorized'):
    # parameters for standwave
    Lx = 10;  Ly = 10
    Nx=10; Ny=10
    T=6
    b=0.0
    dt=0.05

    def I(xv, yv):
	k=zeros((xv.shape[0],yv.shape[1]))
        for i in range(0, length(xv)):
	    for j in range(0, length(yv)):
	        if xv[i,0]<1: 
		    k[i,j]=2.0
	        else: 
		    k[i,j]=0.0 
        return k

    def q(xv, yv):
	g=9.81
	return g*(I(xv,yv)-gaussian(xv,yv))

    def gaussian(xv,yv):
	b1=1.0 # give gaussian circular contour lines when b=1, else elliptical 
	Bmx=5.0 # peak center
	Bmy=7.0 # peak center
	B0=0.0
	Bs=2.0
	Ba=0.3
	return B0+Ba*exp( -((xv-Bmx)/Bs)**2-((yv-Bmy)/(b1*Bs))**2 )

    def plot_u(u, x, xv, y, yv, t, n):
        if t[n] == 0:
            time.sleep(2)
        if plot_method == 1:
            mesh(x, y, u, title='t=%g' % t[n], zlim=[-1,1],
                 caxis=[-1,1])
        elif plot_method == 2:
            surfc(x, y, u, title='t=%g' % t[n], zlim=[-5, 5],
                  colorbar=True, colormap=hot(), caxis=[-5,5],
                  shading='flat')
        if plot_method > 0:
            time.sleep(0) # pause between frames
            filename = 'tmp_%04d.png' % n
            #savefig(filename)  # time consuming - dropped

    # Note: problem with nosetests - some module import problem
    def assert_no_error(u, x, xv, y, yv, t, n):
	plot_u(u, x, xv, y, yv, t, n)
    
    u, xv, yv, t, cpu = solver(I, None, None, q, Lx, Ly, Nx, Ny, dt, T, b,
                         user_action=assert_no_error,
                         version=version)

	


def run_Constant(plot_method=2, version='scalar'):
    """
    Constant solution u=1.2
    plot_method=1 applies mesh function, =2 means surf, =0 means no plot.
    """
    # Clean up plot files
    for name in glob('tmp_*.png'):
        os.remove(name)

    Lx = 5
    Ly = 5
    b=5
    dt=0.2

    def I(x, y):
        """Constant solution value u=1.2"""
        return 1.2

    #def q(i, j):
    #    return 1.0
    q_1=1.0

    def plot_u(u, x, xv, y, yv, t, n):
        if t[n] == 0:
            time.sleep(2)
        if plot_method == 1:
            mesh(x, y, u, title='t=%g' % t[n], zlim=[-1,1],
                 caxis=[-1,1])
        elif plot_method == 2:
            surfc(x, y, u, title='t=%g' % t[n], zlim=[-2, 2],
                  colorbar=True, colormap=hot(), caxis=[-2,2],
                  shading='flat')
        if plot_method > 0:
            time.sleep(0) # pause between frames
            filename = 'tmp_%04d.png' % n
            #savefig(filename)  # time consuming - dropped

    Nx = 60; Ny = 60; T = 2
	            #I, V, f, q_1, Lx, Ly, Nx, Ny, dt, T, b,
    u, dt, cpu = solver(I, None, None, q_1, Lx, Ly, Nx, Ny, dt, T, b,
                user_action=plot_u, version=version)

def run_Constantvec(plot_method=2, version='vectorized'):
    """
    Constant solution u=1.2
    plot_method=1 applies mesh function, =2 means surf, =0 means no plot.
    """
    # Clean up plot files
    for name in glob('tmp_*.png'):
        os.remove(name)

    Lx = 5
    Ly = 5
    b=5
    dt=0.2

    def I(xv, yv):
        """Constant solution value u=1.2"""
	k=zeros((xv.shape[0],yv.shape[1]))
        k[:,:]=1.2 
        return k

    #def q(i, j):
    #    return 1.0
    q_1=1.0

    def plot_u(u, x, xv, y, yv, t, n):
        if t[n] == 0:
            time.sleep(2)
        if plot_method == 1:
            mesh(x, y, u, title='t=%g' % t[n], zlim=[-1,1],
                 caxis=[-1,1])
        elif plot_method == 2:
            surfc(xv, yv, u, title='t=%g' % t[n], zlim=[-2, 2],
                  colorbar=True, colormap=hot(), caxis=[-2,2],
                  shading='flat')
        if plot_method > 0:
            time.sleep(0) # pause between frames
            filename = 'tmp_%04d.png' % n
            #savefig(filename)  # time consuming - dropped

    Nx = 60; Ny = 60; T = 2
	            #I, V, f, q_1, Lx, Ly, Nx, Ny, dt, T, b,
    u, dt, cpu = solver(I, None, None, q_1, Lx, Ly, Nx, Ny, dt, T, b,
                user_action=plot_u, version=version)

def run_1DwaveScalar(plot_method=2, version='scalar'):
    """
    Initial Gaussian bell in the middle of the domain.
    plot_method=1 applies mesh function, =2 means surf, =0 means no plot.
    """
    # Clean up plot files
    for name in glob('tmp_*.png'):
        os.remove(name)

    Lx = 10
    Ly = 10
    b=0
    dt=0.1

    def I(x, y):
        """Constant solution value u=1.2"""
        return 2 if x<2 else 0

    #def q(i, j):
    #    return 1.0
    q_1=1.0

    def plot_u(u, x, xv, y, yv, t, n):
        if t[n] == 0:
            time.sleep(2)
        if plot_method == 1:
            mesh(x, y, u, title='t=%g' % t[n], zlim=[-1,1],
                 caxis=[-1,1])
        elif plot_method == 2:
            surfc(x, y, u, title='t=%g' % t[n], zlim=[-2, 2],
                  colorbar=True, colormap=hot(), caxis=[-2,2],
                  shading='flat')
        if plot_method > 0:
            time.sleep(0) # pause between frames
            filename = 'tmp_%04d.png' % n
            #savefig(filename)  # time consuming - dropped

    Nx = 100; Ny = 100; T = 10
	            #I, V, f, q_1, Lx, Ly, Nx, Ny, dt, T, b,
    u, xv, yv, t, cpu = solver(I, None, None, q_1, Lx, Ly, Nx, Ny, dt, T, b,
                user_action=plot_u, version=version)
    print u

def run_1Dwave(plot_method=2, version='vectorized'):
    """
    Initial Gaussian bell in the middle of the domain.
    plot_method=1 applies mesh function, =2 means surf, =0 means no plot.
    """
    # Clean up plot files
    for name in glob('tmp_*.png'):
        os.remove(name)

    Lx = 10
    Ly = 10
    b=0
    dt=0.1

    def I(xv, yv):
        """Constant solution value u=1.2"""
	k=zeros((xv.shape[0],yv.shape[1]))
        for i in range(0, length(xv)):
	    for j in range(0, length(yv)):
	        if xv[i,0]<1: 
		    k[i,j]=2.0
	        else: 
		    k[i,j]=0.0 
        return k

    #def q(i, j):
    #    return 1.0
    q_1=1.0

    def plot_u(u, x, xv, y, yv, t, n):
        if t[n] == 0:
            time.sleep(2)
        if plot_method == 1:
            mesh(x, y, u, title='t=%g' % t[n], zlim=[-1,1],
                 caxis=[-1,1])
        elif plot_method == 2:
            surfc(xv, yv, u, title='t=%g' % t[n], zlim=[-2, 2],
                  colorbar=True, colormap=hot(), caxis=[-2,2],
                  shading='flat')
        if plot_method > 0:
            time.sleep(0) # pause between frames
            filename = 'tmp_%04d.png' % n
            #savefig(filename)  # time consuming - dropped

    Nx =100; Ny = 100; T = 0.1
	            #I, V, f, q_1, Lx, Ly, Nx, Ny, dt, T, b,
    u, dt, cpu = solver(I, None, None, q_1, Lx, Ly, Nx, Ny, dt, T, b,
                user_action=plot_u, version=version)

    print u[:,:]

if __name__ == '__main__':
    import sys
    from scitools.misc import function_UI
    cmd = function_UI([test_quadratic, run_efficiency_tests,
                       run_Gaussian, run_Test], sys.argv)
    eval(cmd)
