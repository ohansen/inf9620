#!/bin/sh
find . \( -name '*.pvd' -o -name '*.vtu' -o -name '*.png' -o -name '*.eps' -o -name '*~' \) -print -exec rm -f {} \;
