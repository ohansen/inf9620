"""
FEniCS tutorial demo program:
Nonlinear Poisson equation with Dirichlet conditions
in x-direction and homogeneous Neumann (symmetry) conditions
in all other directions. The domain is the unit hypercube in
of a given dimension.

-div(q(u)*nabla_grad(u)) = 0,
u = 0 at x=0, u=1 at x=1, du/dn=0 at all other boundaries.
q(u) = (1+u)^m

Solution method: Picard iteration (successive substitutions).
"""

from dolfin import *
import numpy, sys

# Create mesh and define function space
degree = int(sys.argv[1])
divisions = [int(arg) for arg in sys.argv[2:]]
d = len(divisions)
domain_type = [UnitInterval, UnitSquare, UnitCube]
mesh = domain_type[d-1](*divisions)
if d==1:
    nx = divisions
elif d==2:
    nx, ny = divisions
else:
    nx, ny, nz = divisions
V = FunctionSpace(mesh, 'Lagrange', degree)

# Define boundary conditions
u0 = Expression('exp(-(x[0]*x[0]+x[1]*x[1])/(2*sigma*sigma))', sigma=0.1) # u(x, t, t=0)
f = 0

def boundary(x, on_boundary):  # define the Dirichlet boundary
    return on_boundary

bc = DirichletBC(V, u0, boundary)

def q(u):
    beta=110.0
    return 1+beta*u**2

dt = 0.1
rho=1.0

u = TrialFunction(V)
v = TestFunction(V)
u_1 = interpolate(u0, V)  # previous (known) u
a = u*v*dx + dt/rho*inner(q(u_1)*nabla_grad(u), nabla_grad(v))*dx
L = (u_1 + dt/rho*f)*v*dx
A = assemble(a)   # assemble only once, before the time stepping

# Picard iterations
u = Function(V)     # new unknown function
T=5.		    # Total time
t=dt
while t <=T:
    #u0.t = t
    A, b = assemble_system(a, L, bc)
    solve(A, u.vector(), b)
    u_1.assign(u)   # update for next iteration
    t += dt
print """
Solution of the problem u*v + dt/rho*div(q(u)*nabla_grad(u), nabla_grad(v)) = u_1 + dt/rho*f,
with f=0, q(u) = 1, u=0 at x=0 and u=1 at x=1.
%s
""" % mesh

# Dump solution to the screen
if d==1:
    u_array = u.vector().array()
    if mesh.num_cells() < 1600:
        coor = mesh.coordinates()
        for i in range(len(u_array)):
	    x = coor[i]
            print 'Node %.3f: u = %.4f' % \
                  (x, u_array[i])
elif d==2:
    u_array = u.vector().array()
    if mesh.num_cells() < 1600:
        coor = mesh.coordinates()
        for i in range(len(u_array)):
            x, y = coor[i]
            print 'Node (%.3f,%.3f): u = %.4f' % \
                  (x, y, u_array[i])

# The parameters nx and ny are the number of divisions in each space direction that were used when calling UnitSquare to make the mesh object.

import scitools.BoxField
import scitools.easyviz as ev


u2 = u if u.ufl_element().degree() == 1 else \
     interpolate(u, FunctionSpace(mesh, 'Lagrange', 1))
if d==1:
    x_p = numpy.arange(0, 1.0+1.0/100, 1.0/100)
    u_ex = t*x_p*x_p*(1/2-x_p/3)
    u_box = scitools.BoxField.dolfin_function2BoxField(
            u2, mesh, nx, uniform_mesh=True)
    ev.figure()
    ev.plot(u_box.grid.coorv[0], u_box.values,title='T=%.3f' % T,legend='u')
    ev.hold(True)
    ev.plot(x_p, u_ex, savefig='u_plot.eps',legend='u_e', ylabel='u(x)', xlabel='x')
elif d==2:
    u_box = scitools.BoxField.dolfin_function2BoxField(
            u2, mesh, (nx,ny), uniform_mesh=True)
    X = 0; Y = 1; Z = 0  # convenient indices
    i = nx; j = ny   # upper right corner
    print 'u(%g,%g)=%g' % (u_box.grid.coor[X][i],
                           u_box.grid.coor[Y][j],
                           u_box.values[i,j])
    ev.figure()
    ev.surf(u_box.grid.coorv[X], u_box.grid.coorv[Y], u_box.values,shading='interp', colorbar='on',title='surf plot of u', savefig='u_surf.eps')

# Find max error
u_exact = Expression('t*x[0]*x[0]*(1/2-x[0]/3)', t=t) # I(x0,y0, t=0)
u_e = interpolate(u_exact, V)
diff = numpy.abs(u_e.vector().array() - u.vector().array()).max()
print 'Max error:', diff
e = u_e.vector().array() - u.vector().array()
E = numpy.sqrt(numpy.sum(e**2)/u.vector().array().size)
print 'E', E
