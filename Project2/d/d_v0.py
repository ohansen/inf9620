"""
FEniCS tutorial demo program:
Nonlinear Poisson equation with Dirichlet conditions
in x-direction and homogeneous Neumann (symmetry) conditions
in all other directions. The domain is the unit hypercube in
of a given dimension.

-div(q(u)*nabla_grad(u)) = 0,
u = 0 at x=0, u=1 at x=1, du/dn=0 at all other boundaries.
q(u) = (1+u)^m

Solution method: Picard iteration (successive substitutions).
"""

from dolfin import *
import numpy, sys

# Create mesh and define function space
degree = int(sys.argv[1])
#degree = 1
divisions = [int(arg) for arg in sys.argv[2:]]
d = len(divisions)
domain_type = [UnitInterval, UnitSquare, UnitCube]
mesh = domain_type[d-1](*divisions)
V = FunctionSpace(mesh, 'Lagrange', degree)

# Define boundary conditions

tol = 1E-14
def left_boundary(x, on_boundary):
    return on_boundary and abs(x[0]) < tol

def right_boundary(x, on_boundary):
    return on_boundary and abs(x[0]-1) < tol

Gamma_0 = DirichletBC(V, Constant(0.0), left_boundary)
Gamma_1 = DirichletBC(V, Constant(1.0), right_boundary)
bcs = [Gamma_0, Gamma_1]

# Choice of nonlinear coefficient
m = 2

def q(u):
#    return (1+u)**m
    return 1

u0 = Expression('exp(-pi**2*t)*cos(pi*x[0])', t=0) # I(x0,y0, t=0)

# Define variational problem for Picard iteration
dt = 0.3
rho=1.0

u = TrialFunction(V)
v = TestFunction(V)
u_1 = interpolate(u0, V)  # previous (known) u
a = u*v*dx + dt/rho*inner(q(u_1)*nabla_grad(u), nabla_grad(v))*dx
f = Constant(0.0)
L = (u_1 + dt/rho*f)*v*dx

#A = assemble(a)   # assemble only once, before the time stepping

# Picard iterations
u = Function(V)     # new unknown function
T=2		    # Total time
t=dt

#while t <= T:
#    b = assemble(L)
#    u0.t = t
#    bc.apply(A, b)
#    solve(A, u.vector(), b)
#    u_1.assign(u)

#u_e = interpolate(u0, V)
#maxdiff = numpy.abs(u_e.vector().array()-u.vector().array()).max()
#print 'Max error, t=%.2f: %-10.3f' % (t, maxdiff)
#b = assemble(L, tensor=b)

tol = 1.0E-5        # tolerance
maxiter = 25        # max no of iterations allowed
while t <=T:
    iter = 0            # iteration counter
    eps = 1.0           # error measure ||u-u_1||
    while eps > tol and iter < maxiter:
        iter += 1
    	solve(a == L, u, bcs)
    	diff = u.vector().array() - u_1.vector().array()
    	eps = numpy.linalg.norm(diff, ord=numpy.Inf)
    	print 'iter=%d: norm=%g' % (iter, eps)
    	u_1.assign(u)   # update for next iteration
    convergence = 'convergence after %d Picard iterations for time= %g' % (iter, t)
    if iter >= maxiter:
    	convergence = 'no ' + convergence
    t += dt

print """
Solution of the nonlinear Poisson problem div(q(u)*nabla_grad(u)) = f,
with f=0, q(u) = (1+u)^m, u=0 at x=0 and u=1 at x=1.
%s
%s
""" % (mesh, convergence)

# Find max error
u_exact = Expression('exp(-pi**2*t)*cos(pi*x[0])',t=t)
u_e = interpolate(u_exact, V)
diff = numpy.abs(u_e.vector().array() - u.vector().array()).max()
print 'Max error:', diff
e = u_e.vector().array() - u.vector().array()
E = numpy.sqrt(numpy.sum(e**2)/u.vector().array().size)
print 'E', E
