"""
FEniCS tutorial demo program:
Nonlinear Poisson equation with Dirichlet conditions
in x-direction and homogeneous Neumann (symmetry) conditions
in all other directions. The domain is the unit hypercube in
of a given dimension.

-div(q(u)*nabla_grad(u)) = 0,
u = 0 at x=0, u=1 at x=1, du/dn=0 at all other boundaries.
q(u) = (1+u)^m

Solution method: Picard iteration (successive substitutions).
"""

from dolfin import *
import numpy, sys

# Create mesh and define function space
degree = int(sys.argv[1])
divisions = [int(arg) for arg in sys.argv[2:]]
d = len(divisions)
domain_type = [UnitInterval, UnitSquare, UnitCube]
mesh = domain_type[d-1](*divisions)
nx, ny=divisions
V = FunctionSpace(mesh, 'Lagrange', degree)

# Define boundary conditions
u0 = Expression('exp(-pi*pi*t)*cos(pi*x[0])', t=0) # I(x0,y0, t=0)

def boundary(x, on_boundary):  # define the Dirichlet boundary
    return on_boundary

bc = DirichletBC(V, u0, boundary)

def q(u):
#    return (1+u)**m
    return 1

dt = 0.02**2
rho=1.0

u = TrialFunction(V)
v = TestFunction(V)
u_1 = interpolate(u0, V)  # previous (known) u
a = u*v*dx + dt/rho*inner(q(u_1)*nabla_grad(u), nabla_grad(v))*dx
f = Constant(0.0)
L = (u_1 + dt/rho*f)*v*dx
A = assemble(a)   # assemble only once, before the time stepping

# Picard iterations
u = Function(V)     # new unknown function
T=0.1		    # Total time
t=dt
while t <=T:
    u0.t = t
    A, b = assemble_system(a, L, bc)
    solve(A, u.vector(), b)
    u_1.assign(u)   # update for next iteration
    t += dt
print """
Solution of the problem u*v + dt/rho*div(q(u)*nabla_grad(u), nabla_grad(v)) = u_1 + dt/rho*f,
with f=0, q(u) = 1, u=0 at x=0 and u=1 at x=1.
%s
""" % mesh

# Dump solution to the screen
u_array = u.vector().array()
if mesh.num_cells() < 1600:
    coor = mesh.coordinates()
    for i in range(len(u_array)):
        x, y = coor[i]
        print 'Node (%.3f,%.3f): u = %.4f' % \
              (x, y, u_array[i])

# The parameters nx and ny are the number of divisions in each space direction that were used when calling UnitSquare to make the mesh object.

import scitools.BoxField
u2 = u if u.ufl_element().degree() == 1 else \
     interpolate(u, FunctionSpace(mesh, 'Lagrange', 1))
u_box = scitools.BoxField.dolfin_function2BoxField(
        u2, mesh, (nx,ny), uniform_mesh=True)

X = 0; Y = 1; Z = 0  # convenient indices

i = nx; j = ny   # upper right corner
print 'u(%g,%g)=%g' % (u_box.grid.coor[X][i],
                       u_box.grid.coor[Y][j],
                       u_box.values[i,j])

import scitools.easyviz as ev
ev.figure()
ev.surf(u_box.grid.coorv[X], u_box.grid.coorv[Y], u_box.values,
        shading='interp', colorbar='on',
        title='surf plot of u', savefig='u_surf.eps')

# Find max error
u_exact = Expression('exp(-pi*pi*t)*cos(pi*x[0])', t=t) # I(x0,y0, t=0)
u_e = interpolate(u_exact, V)
diff = numpy.abs(u_e.vector().array() - u.vector().array()).max()
print 'Max error:', diff
e = u_e.vector().array() - u.vector().array()
E = numpy.sqrt(numpy.sum(e**2)/u.vector().array().size)
print 'E', E
